/********************************************************************************************
*                                                                                           *
*                                                                                           *
*                                                                                           *
* MIT License                                                                               *
*                                                                                           *
* Copyright (c) 2020 - 2022 BEAST                                                           *
*                                                                                           *
* Permission is hereby granted, free of charge, to any person obtaining a copy              *
* of this software and associated documentation files (the "Software"), to deal             *
* in the Software without restriction, including without limitation the rights              *
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell                 *
* copies of the Software, and to permit persons to whom the Software is                     *
* furnished to do so, subject to the following conditions:                                  *
*                                                                                           *
* The above copyright notice and this permission notice shall be included in all            *
* copies or substantial portions of the Software.                                           *
*                                                                                           *
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR                *
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,                  *
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE               *
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER                    *
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,             *
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE             *
* SOFTWARE.                                                                                 *
*********************************************************************************************
*/

#pragma once

#include "Pass.h"

#include "Saturn/ImGui/Dockspace.h"

#include <vulkan.h>

namespace Saturn {

	class ImGuiVulkan
	{
	public:
		ImGuiVulkan() { Init(); }
		~ImGuiVulkan() { Terminate(); }
		
		void BeginImGuiRender( VkCommandBuffer CommandBuffer );
		void ImGuiRender();
		void EndImGuiRender();

		void RecreateImages();

		VkCommandBuffer& GetCommandBuffer() { return m_CommandBuffer; }
		VkDescriptorPool& GetDescriptorPool() { return m_DescriptorPool; }

		void* GetOffscreenColorDescSet() { return m_OffscreenID; }

		ImGuiDockspace* GetDockspace() { return m_pDockspace; }

	private:

		VkDescriptorPool m_DescriptorPool = VK_NULL_HANDLE;
		VkCommandBuffer m_CommandBuffer = VK_NULL_HANDLE;
		
		Pass m_ImGuiPass;

		// The current offscreen id, made from the color image and the color sampler.
		void* m_OffscreenID;

		ImGuiDockspace* m_pDockspace;

	private:

		void Init();
		void CreatePipeline();
		void Terminate();

	};
}